package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.entity.TblBantengWidyantoro;

public interface TblBantengWidyantoroRepo extends JpaRepository<TblBantengWidyantoro, String> {

}
