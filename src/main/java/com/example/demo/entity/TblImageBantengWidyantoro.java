package com.example.demo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.sun.istack.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "tbl_image_bantengwidyantoro")
@ToString
public class TblImageBantengWidyantoro {

	@Column(name= "description")
	private byte description;
	
	@Id
	@NotNull
	@Column(name="id_request_booking")
	private String idRequestBooking;
}
