package com.example.demo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.sun.istack.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "tbl_bantengwidyantoro")
@ToString
public class TblBantengWidyantoro {
	
	@Id
	@NotNull
	@Column(name="id_request_booking")
	private String idRequestBooking;
	
	@Column(name= "id_platform", length = 20)
	private String idPlatform;
	
	@Column(name= "nama_platform", length = 20)
	private String namaPlatform;
	
	@Column(name= "doc_type", length = 20)
	private String docType;
	
	@Column(name= "term_of_payment", length = 5)
	private String termOfPayment;
}
