package com.example.demo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@AllArgsConstructor
@NoArgsConstructor
@Data
@ToString
public class InputDto {

	private String idRequestBooking;
	private String idPlatform;
	private String namaPlatform;
	private String docType;
	private String termOfPayment;
}
