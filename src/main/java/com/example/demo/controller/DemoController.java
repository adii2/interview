package com.example.demo.controller;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.InputDto;
import com.example.demo.entity.TblBantengWidyantoro;
import com.example.demo.repository.TblBantengWidyantoroRepo;

@RestController
@RequestMapping("/demo")
public class DemoController {

	@Autowired
	TblBantengWidyantoroRepo tblBantengWidyantoroRepo;
	
	@GetMapping(value = "/get-all", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<TblBantengWidyantoro>> getAll(){
		List<TblBantengWidyantoro> list = tblBantengWidyantoroRepo.findAll();
		return new ResponseEntity<List<TblBantengWidyantoro>>(list, HttpStatus.OK);
    }
	
	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TblBantengWidyantoro> getById(@PathVariable(name = "id") String id){
		TblBantengWidyantoro tblBantengWidyantoro = tblBantengWidyantoroRepo.findById(id).get();
        return new ResponseEntity<TblBantengWidyantoro>(tblBantengWidyantoro, HttpStatus.OK);
    }
	
	@PostMapping(value = "/create", produces = MediaType.TEXT_PLAIN_VALUE)
	@Transactional
	public ResponseEntity<String> create(@RequestBody InputDto inputDto) {
		
		try {
			TblBantengWidyantoro tblBantengWidyantoro = new TblBantengWidyantoro(inputDto.getIdRequestBooking(), inputDto.getIdPlatform(), inputDto.getNamaPlatform(), inputDto.getDocType(), inputDto.getTermOfPayment());
			tblBantengWidyantoroRepo.save(tblBantengWidyantoro);
			
			return new ResponseEntity<String>("CREATED", HttpStatus.CREATED);
		} catch(Exception e) {
			e.printStackTrace();
			return new ResponseEntity<String>("FAIL CREATED", HttpStatus.BAD_REQUEST);
		}
        
    }
	
	@PutMapping(value = "/{id}/update", produces = MediaType.TEXT_PLAIN_VALUE)
	@Transactional
	public ResponseEntity<String> update(@PathVariable(name = "id") String id, @RequestBody InputDto inputDto) {
		
		try {
			TblBantengWidyantoro tblBantengWidyantoro = tblBantengWidyantoroRepo.findById(id).get();
			if(null == tblBantengWidyantoro) {
				return new ResponseEntity<String>("Data not found!", HttpStatus.BAD_REQUEST);
			}
			tblBantengWidyantoro.setNamaPlatform(inputDto.getNamaPlatform());
			tblBantengWidyantoro.setDocType(inputDto.getDocType());
			tblBantengWidyantoro.setIdPlatform(inputDto.getIdPlatform());
			tblBantengWidyantoro.setTermOfPayment(inputDto.getTermOfPayment());
			tblBantengWidyantoroRepo.save(tblBantengWidyantoro);
			
			return new ResponseEntity<String>("UPDATED", HttpStatus.OK);
		} catch(Exception e) {
			e.printStackTrace();
			return new ResponseEntity<String>("FAIL UPDATED", HttpStatus.BAD_REQUEST);
		}
        
    }
	
	@DeleteMapping(value = "/{id}/delete", produces = MediaType.TEXT_PLAIN_VALUE)
	public ResponseEntity<String> delete(@PathVariable(name = "id") String id) {
		
		try {
			TblBantengWidyantoro tblBantengWidyantoro = tblBantengWidyantoroRepo.findById(id).get();
			if(null == tblBantengWidyantoro) {
				return new ResponseEntity<String>("Data not found!", HttpStatus.BAD_REQUEST);
			}
			
			tblBantengWidyantoroRepo.delete(tblBantengWidyantoro);
			
			return new ResponseEntity<String>("DELETED", HttpStatus.OK);
		} catch(Exception e) {
			e.printStackTrace();
			return new ResponseEntity<String>("FAIL DELETED", HttpStatus.BAD_REQUEST);
		}
        
    }
}
